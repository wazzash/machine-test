import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Utils {
  static Future<bool> displayToast(String message, int length) {
    return Fluttertoast.showToast(
        msg: message,
        toastLength: length == 0 ? Toast.LENGTH_SHORT : Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 2,
        backgroundColor: const Color(0x85000000),
        textColor: Colors.white,
        fontSize: 14.0);
  }

  static void displaySnackBar(String message, BuildContext context,
      {String actionMessage, VoidCallback onClick}) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(
        message,
        style: TextStyle(color: Colors.white, fontSize: 14.0),
      ),
      action: (actionMessage != null)
          ? SnackBarAction(
        textColor: Colors.pink,
        label: actionMessage,
        onPressed: () {
          if (onClick != null)
            return onClick();
          else
            ScaffoldMessenger.of(context).hideCurrentSnackBar();
        },
      )
          : null,
      duration: Duration(seconds: 10),
      // backgroundColor: const Color(0x854E4E4E),
    ));
  }
}
