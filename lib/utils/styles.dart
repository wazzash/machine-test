import 'package:flutter/material.dart';

final titleStyle = TextStyle(
  color: Colors.black,
  fontFamily: 'CM Sans Serif',
  fontSize: 18.0,
  // height: 1.5,
);

final boldNameStyle = TextStyle(
  color: Colors.black,
  fontFamily: 'CM Sans Serif',
  fontSize: 13.0,
  // height: 1.5,
);

final subtitleStyle = TextStyle(
  color: Colors.black.withOpacity(0.8),
  fontSize: 13.0,
  // height: 1.2,
);