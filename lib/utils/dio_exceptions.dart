import 'package:dio/dio.dart';

class DioExceptions implements Exception {
  @override
  String toString() => message;

  DioExceptions.fromDioError(e) {
    if (e is DioError) {
      switch (e.type) {
        case DioErrorType.cancel:
          message = "Request to API server was cancelled";
          break;
        case DioErrorType.connectTimeout:
          message = "Connection timeout with API server";
          break;
        case DioErrorType.other:
          message =
          "Connection to API server failed. Check internet connection";
          print("abc_dio_error:  $e");
          break;
        case DioErrorType.receiveTimeout:
          message = "Receive timeout in connection with API server";
          break;
        case DioErrorType.response:
          message = _handleError(e.response.statusCode, e.response.data);
          break;
        case DioErrorType.sendTimeout:
          message = "Send timeout in connection with API server";
          break;
        default:
          message = "Something went wrong";
          break;
      }
    } else {
      // message = e.toString();
      print('abc_exceptionNonDio: $e');
    }
  }

  String message;

  String _handleError(int statusCode, dynamic error) {
    switch (statusCode) {
      case 400:
        return 'Bad request';
      case 404:
        print('$statusCode + $error');
        return '404 Not Found';
      case 500:
        return 'Internal server error';
      default:
        return 'Oops something went wrong - error $statusCode';
    }
  }
}
