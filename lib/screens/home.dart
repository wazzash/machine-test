import 'package:flutter/material.dart';
import 'package:riafy_machine_test_fawaz/models/home_model.dart';
import 'package:riafy_machine_test_fawaz/screens/feeds.dart';
import 'package:riafy_machine_test_fawaz/utils/const.dart';
import 'package:riafy_machine_test_fawaz/utils/dio_exceptions.dart';
import 'dart:async';
import 'dart:io';
import 'dart:math' as math;
import 'dart:convert';

class Home extends StatefulWidget {
  _Home createState() => _Home();
}

class _Home extends State<Home> with AutomaticKeepAliveClientMixin<Home> {
  List<Posts> homeData = List.empty();

  @override
  void initState() {
    super.initState();
    this._getFeed();
  }

  buildFeed() {
    if (homeData != null) {
      return Container(
          height: double.infinity,
          child:
          // homeData.length != 0
          //     ?
          SingleChildScrollView(
                  // physics: BouncingScrollPhysics(),
                  physics: AlwaysScrollableScrollPhysics(),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: homeData
                        .map((homeFeeds) => FeedList(
                              posts: homeFeeds,
                            ))
                        .toList(),
                  ),
                )
              // : Center(
              //     child: Container(
              //       height: MediaQuery.of(context).size.height * 0.75,
              //       child: Center(
              //         child: Text(
              //           'Something went wrong!',
              //           textAlign: TextAlign.center,
              //           style: TextStyle(
              //             fontSize: 18,
              //             fontFamily: 'CM Sans Serif',
              //             fontWeight: FontWeight.bold,
              //             color: Colors.black.withOpacity(0.8),
              //             fontStyle: FontStyle.normal,
              //           ),
              //         ),
              //       ),
              //     ),
              //   )
    );
    } else {
      return Container(
          alignment: FractionalOffset.center,
          child: CircularProgressIndicator());
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context); // reloads state when opened again

    return Scaffold(
      appBar: AppBar(
        title: const Text('Riagram',
            style: const TextStyle(
                fontFamily: "Billabong", color: Colors.black, fontSize: 35.0)),
        // centerTitle: true,
        leading: IconButton(
          icon: Icon(
            Icons.camera_alt_outlined,
            color: Colors.black,
          ),
          onPressed: () {
            // do something
          },
        ),
        actions: <Widget>[
          Transform.rotate(
            angle: 330 * math.pi / 180,
            child: IconButton(
              icon: Icon(
                Icons.send_outlined,
                color: Colors.black,
              ),
              onPressed: null,
            ),
          ),
        ],
        backgroundColor: Colors.white,
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: buildFeed(),
      ),
    );
  }

  Future<Null> _refresh() async {
    await _getFeed();

    setState(() {});

    return;
  }

  _getFeed() async {
    print("Staring getFeed");

    var url =
        'https://hiit.ria.rocks/videos_api/cdn/com.rstream.crafts?versionCode=40&lurl=Canvas%20painting%20ideas' ;
    var httpClient = HttpClient();

    // List<ImagePost> listOfPosts;
    String result;
    try {
      var request = await httpClient.getUrl(Uri.parse(url));
      var response = await request.close();
      if (response.statusCode == HttpStatus.ok) {
        String json = await response.transform(utf8.decoder).join();
        final _data = jsonDecode(json);
          print("abc_home: $json");
        // prefs.setString("feed", json);
        // List<Map<String, dynamic>> data =
        // jsonDecode(json).cast<Map<String, dynamic>>();
        // listOfPosts = _generateFeed(data);
        HomeList homeList = new HomeList.fromJson(_data);
          setState(() {
            homeData = homeList.feeds;
          });
        result = "Success in http request for feed";
      } else {
        result =
        'Error getting a feed: Http status ${response.statusCode}';
      }
    } catch (exception) {
      result = 'Failed invoking the getFeed function. Exception: $exception';
    }
    print(result);

    setState(() {
    });


    // Dio dio = new Dio();
    // try {
    //   Response response = await dio.get(
    //     "https://hiit.ria.rocks/videos_api/cdn/com.rstream.crafts?versionCode=40&lurl=Canvas%20painting%20ideas",
    //   );
    //
    //   final data = json.decode(response.toString());
    //   print("abc_home: $data");
    //
    //   HomeList homeList = new HomeList.fromJson(data);
    //   setState(() {
    //     homeData = homeList.feeds;
    //   });
    // } catch (e) {
    //   final errorMessage = DioExceptions.fromDioError(e).toString();
    //   if (errorMessage != null) Utils.displayToast(errorMessage, 0);
    // }
  }

  // ensures state is kept when switching pages
  @override
  bool get wantKeepAlive => true;
}
