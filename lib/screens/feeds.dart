import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:progressive_image/progressive_image.dart';
import 'package:riafy_machine_test_fawaz/models/home_model.dart';
import 'package:riafy_machine_test_fawaz/utils/styles.dart';

class FeedList extends StatelessWidget {
  final Posts posts;

  FeedList({this.posts});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          leading: CircleAvatar(
            backgroundImage: NetworkImage(posts.lowThumb),
            backgroundColor: Colors.grey,
          ),
          title: GestureDetector(
            child: Text(posts.name, style: titleStyle),
            onTap: () {},
          ),
          trailing: const Icon(Icons.more_vert),
        ),
        GestureDetector(
          onDoubleTap: () => null, //like image
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              ProgressiveImage(
                placeholder: AssetImage('assets/images/placeholder.png'),
                thumbnail: NetworkImage(posts.medThumb),
                image: NetworkImage(posts.highThumb),
                fit: BoxFit.fitWidth,
                // height: 230,
                // width: double.infinity,
              ),
              // CachedNetworkImage(
              //   imageUrl: posts.highThumb,
              //   fit: BoxFit.fitWidth,
              //   placeholder: (context, url) => loadingPlaceHolder,
              //   errorWidget: (context, url, error) => Icon(Icons.error),
              // ),
            ],
            // children: <Widget>[
            //   CachedNetworkImage(
            //     imageUrl: mediaUrl,
            //     fit: BoxFit.fitWidth,
            //     placeholder: (context, url) => loadingPlaceHolder,
            //     errorWidget: (context, url, error) => Icon(Icons.error),
            //   ),
            //   showHeart
            //       ? Positioned(
            //           child: Container(
            //             width: 100,
            //             height: 100,
            //             child: Opacity(
            //                 opacity: 0.85,
            //                 child: FlareActor(
            //                   "assets/flare/Like.flr",
            //                   animation: "Like",
            //                 )),
            //           ),
            //         )
            //       : Container()
            // ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  GestureDetector(
                      child: IconButton(
                          icon: FaIcon(FontAwesomeIcons.heart),
                          onPressed: () {})),
                  GestureDetector(
                      child: IconButton(
                          icon: FaIcon(FontAwesomeIcons.comment),
                          onPressed: () {})),
                  GestureDetector(
                      child: IconButton(
                          icon: FaIcon(FontAwesomeIcons.share),
                          onPressed: () {})),
                  new Spacer(),
                  GestureDetector(
                      child: IconButton(
                          icon: FaIcon(FontAwesomeIcons.bookmark),
                          onPressed: () {})),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                      child: Text(
                    posts.name,
                    style: boldNameStyle,
                  )),
                  SizedBox(
                    width: 5,
                  ),
                  Expanded(
                      child: Text(
                    posts.title,
                    style: subtitleStyle,
                  )),
                ],
              )
            ],
          ),
        ),
        SizedBox(
          height: 20,
        )
      ],
    );
  }

  Container loadingPlaceHolder = Container(
    height: 400.0,
    child: Center(child: CircularProgressIndicator()),
  );
}
