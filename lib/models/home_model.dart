class HomeList {
  final List<Posts> feeds;

  HomeList({
    this.feeds,
  });

  factory HomeList.fromJson(List<dynamic> parsedJson) {
    List<Posts> feeds = parsedJson.map((i) => Posts.fromJson(i)).toList();

    return new HomeList(feeds: feeds);
  }
}

class Posts {
  final String id;
  final String name;
  final String title;
  final String lowThumb;
  final String medThumb;
  final String highThumb;

  Posts(
      {this.id,
      this.name,
      this.title,
      this.lowThumb,
      this.medThumb,
      this.highThumb});

  factory Posts.fromJson(Map<String, dynamic> parsedJson) {
    print("abc_model");
    return new Posts(
      id: parsedJson['id'],
      name: parsedJson['channelname'],
      title: parsedJson['title'],
      lowThumb: parsedJson['low thumbnail'],
      medThumb: parsedJson['medium thumbnail'],
      highThumb: parsedJson['high thumbnail'],
    );
  }
}
